/**
 * SEO
 * @author Chen Liangqun <http://topphp.org>
 * @copyright Copyright (c) 2013, Chen Liangqun <it@topphp.org>
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 1.0.0 - Build20130415
 *
 * @file
 * Javascript behaviors for the SEO module.
 */

(function ($) {
  Drupal.behaviors.seoFieldsetSummaries = {
    attach: function (context) {
      $('fieldset.seo-settings-form', context).drupalSetSummary(function (context) {
        if ($('.seo-settings-title').is(':focus')) {
          return Drupal.t('SEO设置：标题');
        }

        if ($('.seo-settings-keywords').is(':focus')) {
          return Drupal.t('SEO设置：关键字');
        }

        if ($('.seo-settings-description').is(':focus')) {
          return Drupal.t('SEO设置：描述');
        }

        return Drupal.t('SEO设置');
      });
    }
  };
})(jQuery);
