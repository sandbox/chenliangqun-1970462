SEO  - Drupal 7

开启SEO模块，可以对相应内容类型的文章进行title、keywords、description设置。
若指定title（这里的title有别于文章的title），则其作用于<head>标签的<title>标签。
若指定keywords，则其作用于<meta name="keywords" />中。
若指定description，则其作用于<meta name="description" />中。
同时，可以对首页进行title、keywords、description设置。
